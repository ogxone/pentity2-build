<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.04.2015
 * Time: 12:39
 */

namespace Pentity2\Build\Cache\Factory;


use Pentity2\Build\Cache\Component\AggregateCleaner;
use Pentity2\Build\Cache\Exception\ClearCacheRuntimeException;
use Pentity2\Build\Cache\Listener\CacheCleanerListener;
use Pentity2\Utils\Param\Param;
use Zend\EventManager\EventManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Pentity2\Build\Cache\Component\CacheCleanerInterface;

class CacheCleanerFactory implements CacheCleanerFactoryInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private $_namespaces = [
        'Pentity2\Build\Cache\Component\Components',
        'Build\Infrastructure\Service\Cache\Component\Components'
    ];

    public function createCacheCleaner(Array $options)
    {
        $sl = $this->getServiceLocator();
        $config = $sl->get('Config');
        $events = new EventManager;
        $events->attachAggregate($sl->get('Build\CacheCleanerListener'));
        $sl->setService('CacheCleanerEventManager', $events);
        $blocks = Param::getArr('clear_blocks', $options);
        $specs = Param::getArr('cache_cleaner', $config);
        if (!empty($blocks)) {  // strip blocks we not selected
            $this->_stripBlocks($specs, $blocks);
        } else {                // if clear all ->> strip not default items
            $this->_stripNotDefaults($specs);
        }

        $cleaners = new AggregateCleaner;
        foreach ($specs as $spec) {
            try {
                $name = Param::strict('name', $spec);
                $params = Param::strictArr('params', $spec);

                $cleaner = $this->_createConcreteCleaner($name, array_merge($params));
                if ($cleaner instanceof ServiceLocatorAwareInterface) {
                    $cleaner->setServiceLocator($sl);
                }
                $cleaner->setEventManager($events);
                $cleaners->add($cleaner);
            } catch (\RuntimeException $e) {
                $events->trigger(CacheCleanerListener::EVENT_CLEAR_INIT_FAILURE, null, [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]);
            }
        }
        if ($cleaners->isEmpty()) {
            $events->trigger(CacheCleanerListener::EVENT_CLEAR_INIT_FAILURE, $this, [ 'message' => 'Nothing to clean']);
            return false;
        }
        return $cleaners;
    }

    /**
     * @param $name
     * @param array $params
     * @throws ClearCacheRuntimeException
     * @return CacheCleanerInterface
     */
    private function _createConcreteCleaner($name, Array $params)
    {
        $className = ucfirst($name) . 'Cleaner';
        foreach ($this->_namespaces as $namespace) {
            if (class_exists($fullClassName = $namespace . '\\' . $className)) {
                $ref = new \ReflectionClass($fullClassName);
                return $ref->newInstanceArgs($params);
            }
        }
        throw new ClearCacheRuntimeException(sprintf('Undefined cleaner type %s', $name));
    }

    private function _stripBlocks(Array &$specs, Array $blocks)
    {
        $specs = array_intersect_key($specs, array_flip($blocks));
    }

    private function _stripNotDefaults(Array &$specs)
    {
        $specs = array_filter($specs, function($v){
            if (isset($v['default']) && !$v['default']) {
                return false;
            }
            return true;
        });
    }
}