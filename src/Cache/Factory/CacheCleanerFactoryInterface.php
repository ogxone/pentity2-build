<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 17:40
 */

namespace Pentity2\Build\Cache\Factory;


interface CacheCleanerFactoryInterface 
{
    public function createCacheCleaner(Array $options);
}