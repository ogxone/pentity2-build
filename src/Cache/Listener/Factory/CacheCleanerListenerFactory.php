<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 24.04.2015
 * Time: 12:46
 */

namespace Pentity2\Build\Cache\Listener\Factory;

use Pentity2\Build\Cache\Listener\CacheCleanerListener;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CacheCleanerListenerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new CacheCleanerListener(
            $serviceLocator->get('Log\BuildLogger'),
            $serviceLocator->get('Log\BuildErrorLogger')
        );
    }
}