<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.04.2015
 * Time: 12:32
 */

namespace Pentity2\Build\Cache\Listener;

use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Log\Logger;

class CacheCleanerListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $_logger;
    private $_loggerError;
    private $_eol;

    const EVENT_CLEAR_DONE = 'clear_done';
    const EVENT_CLEAR_NOTICE = 'clear_notice';
    const EVENT_CLEAR_WARNING = 'clear_warning';
    const EVENT_CLEAR_INFO = 'clear_info';
    const EVENT_CLEAR_FAILURE = 'clear_failure';
    const EVENT_CLEAR_INIT_FAILURE = 'clear_init_failure';

    public function __construct(Logger $logger, Logger $loggerError)
    {
        $this->_eol = false !== strpos(php_sapi_name(), 'cli') ? PHP_EOL  : '<br/>';
        $this->_logger = $logger;
        $this->_loggerError = $loggerError;
    }

    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(self::EVENT_CLEAR_DONE, [$this, 'clearDone']);
        $this->listeners[] = $events->attach(self::EVENT_CLEAR_NOTICE, [$this, 'clearNotice']);
        $this->listeners[] = $events->attach(self::EVENT_CLEAR_WARNING, [$this, 'clearWarning']);
        $this->listeners[] = $events->attach(self::EVENT_CLEAR_INFO, [$this, 'clearInfo']);
        $this->listeners[] = $events->attach(self::EVENT_CLEAR_FAILURE, [$this, 'clearFailure']);
        $this->listeners[] = $events->attach(self::EVENT_CLEAR_INIT_FAILURE, [$this, 'clearInitFailure']);
    }

    public function clearDone(EventInterface $event)
    {
        $this->_logger->log(
            Logger::INFO,
            sprintf('CLEAR DONE! Component %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message')) . $this->_eol
        );
    }

    public function clearNotice(EventInterface $event)
    {
        $this->_logger->log(
            Logger::NOTICE,
            sprintf('CLEAR NOTICE! Component %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message')) . $this->_eol
        );
    }

    public function clearWarning(EventInterface $event)
    {
        $this->_logger->log(
            Logger::NOTICE,
            sprintf('CLEAR WARNING! Component %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message')) . $this->_eol
        );
    }

    public function clearInfo(EventInterface $event)
    {
        $this->_logger->log(
            Logger::INFO,
            sprintf('CLEAR INFO! Component %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message')) . $this->_eol
        );
    }

    public function clearFailure(EventInterface $event)
    {
        $this->_logger->log(
            Logger::ERR,
            sprintf('CLEAR FAILURE! Component %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message')) . $this->_eol
        );
    }

    public function clearInitFailure(EventInterface $event)
    {
        $this->_logger->log(
            Logger::ERR,
            sprintf('CLEAR INIT FAILURE! Message: %s', $event->getParam('message')) . $this->_eol
        );
    }
}