<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 15:55
 */

namespace Pentity2\Build\Cache\Component\Components;


use Pentity2\Build\Cache\Component\AbstractCacheCleaner;
use Pentity2\Build\Cache\Exception\ClearCacheException;
use Pentity2\Infrastructure\Cache\TaggableCache;

class TagCleaner extends AbstractCacheCleaner
{
    private $_tags;

    public function __construct($serviceName, Array $tags)
    {
        parent::__construct($serviceName);
        $this->_tags = $tags;
    }

    protected function _clearCacheLogic()
    {
        $cacheService = $this->_retrieveCacheService($this->_serviceName);
        if (!$cacheService instanceof TaggableCache) {
            throw new ClearCacheException(sprintf('%s doesn\'t implement Pentity2\Infrastructure\Cache\TaggableCache interface'));
        }
        $cacheService->clearByTags($this->_tags);
    }
}