<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 15:55
 */

namespace Pentity2\Build\Cache\Component\Components;


use Pentity2\Build\Cache\Component\AbstractCacheCleaner;
use Pentity2\Utils\Filesystem\Utils;

class FileCleaner extends AbstractCacheCleaner
{
    private $_paths = [];

    public function __construct($serviceName, Array $paths)
    {
        parent::__construct($serviceName);
        $this->_paths = $paths;
    }

    protected function _clearCacheLogic()
    {
        foreach ($this->_paths as $path) {
            try {
                if (!file_exists($path)) {
                    throw new \UnexpectedValueException(sprintf('File %s is not exists', $path));
                }
                if (is_dir($path)) {
                    Utils::clearDir($path);
                } else {
                    Utils::unlink($path);
                }
            } catch (\UnexpectedValueException $e) {
                $this->_notice($e->getMessage());
            }
        }
    }
}