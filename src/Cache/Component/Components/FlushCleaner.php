<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 15:55
 */

namespace Pentity2\Build\Cache\Component\Components;


use Pentity2\Build\Cache\Component\AbstractCacheCleaner;
use Pentity2\Build\Cache\Exception\ClearCacheException;
use Pentity2\Infrastructure\Decorator\DecoratorInterface;
use Zend\Cache\Storage\FlushableInterface;

class FlushCleaner extends AbstractCacheCleaner
{
    public function __construct($serviceName)
    {
        parent::__construct($serviceName);
    }

    protected function _clearCacheLogic()
    {
        $cacheService = $this->_retrieveCacheService($this->_serviceName);

        if ($cacheService instanceof DecoratorInterface) {
            $cache = $cacheService->getOrigin();
        } else {
            $cache = $cacheService;
        }

        if (!$cache instanceof FlushableInterface) {
            throw new ClearCacheException(sprintf('%s doesn\'t implement Zend\Cache\Storage\FlushableInterface interface', get_class($cacheService)));
        }
        $cache->flush();
    }
}