<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 15:55
 */

namespace Pentity2\Build\Cache\Component\Components;


use Pentity2\Build\Cache\Component\AbstractCacheCleaner;
use Pentity2\Build\Cache\Exception\ClearCacheException;

use Pentity2\Infrastructure\Decorator\DecoratorInterface;
use Zend\Cache\Storage\ClearByNamespaceInterface;


class NamespaceCleaner extends AbstractCacheCleaner
{
    private $_namespace;

    public function __construct($serviceName, $namespace)
    {
        parent::__construct($serviceName);
        $this->_namespace = $namespace;
    }

    protected function _clearCacheLogic()
    {
        $cacheService = $this->_retrieveCacheService($this->_serviceName);
        if ($cacheService instanceof DecoratorInterface) {
            $cache = $cacheService->getOrigin();
        } else {
            $cache = $cacheService;
        }

        if (!$cache instanceof ClearByNamespaceInterface) {
            throw new ClearCacheException(sprintf(
                'Service %s doesn\'t implement Zend\Cache\Storage\ClearByNamespaceInterface interface', get_class($cache)));
        }
        $cache->clearByNamespace($this->_namespace);
    }
}