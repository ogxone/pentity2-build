<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 15:55
 */

namespace Pentity2\Build\Cache\Component\Components;


use Pentity2\Build\Cache\Component\AbstractCacheCleaner;

class ItemCleaner extends AbstractCacheCleaner
{
    private $_items;

    public function __construct($serviceName, Array $items)
    {
        parent::__construct($serviceName);
        $this->_items = $items;
    }

    protected function _clearCacheLogic()
    {
        $cacheService = $this->_retrieveCacheService($this->_serviceName);
        $cacheService->removeItems($this->_items);
    }
}