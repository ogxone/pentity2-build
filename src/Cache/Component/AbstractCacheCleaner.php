<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.04.2015
 * Time: 12:30
 */

namespace Pentity2\Build\Cache\Component;


use Pentity2\Build\Cache\Exception\ClearCacheException;
use Pentity2\Build\Cache\Exception\ClearCacheRuntimeException;
use Pentity2\Build\Cache\Listener\CacheCleanerListener;
use Pentity2\Build\DescriptionAwareInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\ArrayUtils;
use Zend\Cache\Storage\StorageInterface as ZendStorageInterface;
use Pentity2\Infrastructure\Cache\StorageInterface;

abstract class AbstractCacheCleaner implements
    CacheCleanerInterface, ServiceLocatorAwareInterface, EventManagerAwareInterface, DescriptionAwareInterface
{
    use EventManagerAwareTrait, ServiceLocatorAwareTrait;

    protected $_serviceName;

    public function __construct($serviceName)
    {
        $this->_serviceName = $serviceName;
    }

    protected function _notice($message)
    {
        $this->_trig(CacheCleanerListener::EVENT_CLEAR_NOTICE, $message);
    }

    protected function _info($message)
    {
        $this->_trig(CacheCleanerListener::EVENT_CLEAR_INFO, $message);
    }

    protected function _failure($message)
    {
        $this->_trig(CacheCleanerListener::EVENT_CLEAR_FAILURE, $message);
    }

    protected function _trig($event, $message, \Exception $exception = null)
    {
        $this->getEventManager()
            ->trigger($event, $this, [
                'message' => $message,
                'exception' => $exception
            ]);
    }

    public function clearCache()
    {
        $em = $this->getEventManager();
        try {
            $em->trigger(CacheCleanerListener::EVENT_CLEAR_INFO, $this, ['message' => 'Starting clear cache ...']);
            $this->_clearCacheLogic();
            $em->trigger(CacheCleanerListener::EVENT_CLEAR_DONE, $this, ['message' => 'OK']);
        } catch(ClearCacheRuntimeException $e) {
            $em->trigger(CacheCleanerListener::EVENT_CLEAR_WARNING, $this, ['message' => $e->getMessage()]);
        } catch (ClearCacheException $e) {
            $em->trigger(CacheCleanerListener::EVENT_CLEAR_FAILURE, $this, ['message' => $e->getMessage()]);
        }
    }

    abstract protected function _clearCacheLogic();
    public function getDescription()
    {
        return $this->_serviceName;
    }

    protected function _retrieveCacheService($cacheServiceName)
    {
        if (!$this->getServiceLocator()->has($cacheServiceName)) {
            throw new ClearCacheException(sprintf('Undefined components %s', $cacheServiceName));
        }
        $cacheService = $this->getServiceLocator()->get($cacheServiceName);
        if ($cacheService instanceof ZendStorageInterface || $cacheService instanceof StorageInterface) {
            return $cacheService;
        }
        throw new ClearCacheException(sprintf('Disallowed service type %s', get_class($this->_serviceName)));
    }
}