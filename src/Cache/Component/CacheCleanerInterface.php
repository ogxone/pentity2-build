<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.04.2015
 * Time: 12:29
 */

namespace Pentity2\Build\Cache\Component;

interface CacheCleanerInterface
{
    public function clearCache();
} 