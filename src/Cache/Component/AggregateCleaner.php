<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 17.06.15
 * Time: 17:25
 */

namespace Pentity2\Build\Cache\Component;


use Pentity2\Build\Cache\Exception\ClearCacheException;
use Pentity2\Utils\Collection\Collection;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AggregateCleaner extends Collection implements CacheCleanerInterface
{
    use ServiceLocatorAwareTrait;

    public function clearCache()
    {
        foreach ($this as $cleaner) {
            $cleaner->clearCache();
        }
    }

    public function getDescription()
    {
        throw new ClearCacheException('Not implemented yet');
    }
}