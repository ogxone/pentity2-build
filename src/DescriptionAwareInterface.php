<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 15:44
 */

namespace Pentity2\Build;


interface DescriptionAwareInterface
{
    public function getDescription();
}