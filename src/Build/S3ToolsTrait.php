<?php
/**
 * Created by PhpStorm.
 * User: Макс
 * Date: 07.09.2015
 * Time: 15:39
 */

namespace Pentity2\Build\Build;

use Pentity2\Utils\Param\Param;

trait S3ToolsTrait {

    public function syncFolder2S3($path, $type, $gzip = false, $alterType = false)
    {
        $s3Config = Param::getArr('aws_s3', $this->_getConfig());
        exec(
            $s3Config['s3cmd']
            . ' --config=' . $s3Config['cfg']
            . ' sync '
            . ' --add-header=\'Expires:' . gmdate('r', time() + $s3Config['cache']) . '\' '
            . ($gzip ? ' --add-header=Content-Encoding:gzip ' : ' ')
            . ' --no-preserve '
            . ' ' . $path . '/' . $type
            . '/ s3://' . $s3Config['bucket'] . $s3Config['storage']['base_path'] . ($alterType ?: $type) . '/'
        );
    }
} 