<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 23.04.2015
 * Time: 23:45
 */

namespace Pentity2\Build\Build\Factory;


interface BuildFactoryInterface
{
    public function createBuild($environment, Array $options);
} 