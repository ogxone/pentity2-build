<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 23.04.2015
 * Time: 23:44
 */

namespace Pentity2\Build\Build\Factory;


use Pentity2\Utils\ArrayUtils\ArrayUtils;
use Pentity2\Utils\ErrorHandler\ErrorHandler;
use Pentity2\Utils\Param\Param;
use Pentity2\Build\Build\Component\AggregateBuilder;
use Pentity2\Build\Build\Exception\BuildException;
use Pentity2\Build\Build\Listener\BuildListener;
use Zend\EventManager\EventManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;


class BuildFactory implements BuildFactoryInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait, ErrorHandler;

    public function createBuild($environment, Array $options = [])
    {
        $sl = $this->getServiceLocator();
        $config = $sl->get('Config');
        $events = new EventManager;
        $events->attachAggregate($sl->get('Build\BuildListener'));
        $sl->setService('BuildEventManager', $events);
        try {
            $specs = Param::strictArr('build', $config, 'Build config is empty. Nothing to build');
            $envSpecs = Param::getArr($environment, $specs);
            $common = Param::getArr('common', $specs);
            $mergedSpecs = ArrayUtils::merge($common, $envSpecs);
            $this->_stripBlocks($mergedSpecs, Param::getArr('build_blocks', $options));
            $this->_sanitalizeSpecs($mergedSpecs);
            $this->_sortSpecsByPriorities($mergedSpecs);
            return $this->_createBuilder($mergedSpecs, $environment);
        } catch (\Exception $e) {
            $events->trigger(BuildListener::EVENT_BUILD_FAILURE, $this, [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]);
            return false;
        }
    }

    private function _createBuilder(Array $specs, $environment)
    {
        $builder = new AggregateBuilder;
        $componentFactory = $this->getServiceLocator()->get('Build\ComponentFactory');
        foreach ($specs as $name => $spec) {
            try {
                $builder->add($componentFactory->createBuildComponent($name, $spec['name'], $environment));
            } catch (BuildException $e) {
                $this->getServiceLocator()
                    ->get('BuildEventManager')
                    ->trigger(BuildListener::EVENT_BUILD_FAILURE, $this, [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]);
            }
        }
        return $builder;
    }

    private function _sortSpecsByPriorities(Array &$specs)
    {
        uasort($specs, function($a, $b){
            if ($a == $b) {
                return 0;
            }
            return ($a['seq'] < $b['seq']) ? 1 : -1;
        });
    }

    private function _sanitalizeSpecs(Array &$specs)
    {
        foreach ($specs as $name => &$spec) {
            if (!is_array($spec) || !isset($spec['name'])) {
                $this->getServiceLocator()
                    ->get('BuildEventManager')
                    ->trigger(BuildListener::EVENT_BUILD_FAILURE, $this, ['message' => sprintf('Badly defined build option %s', $name)]);
                unset($specs[$name]);
                continue;
            }
            if (!isset($spec['seq'])) {
                $spec['seq'] = 0;
            }
        }
    }

    private function _stripBlocks(Array &$specs, Array $blocks)
    {
        if (empty($blocks)) {
            return;
        }
        foreach ($specs as $name => $spec) {
            if (false === array_search($name, $blocks)) {
                unset($specs[$name]);
            }
        }
    }
}