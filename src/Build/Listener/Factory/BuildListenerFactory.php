<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 24.04.2015
 * Time: 12:46
 */

namespace Pentity2\Build\Build\Listener\Factory;


use Formatter\Formatters\CliFormatter;
use Formatter\Formatters\ColorFormatter;
use Formatter\Formatters\CompositeFormatter;
use Formatter\Formatters\HtmlFormatter;
use Pentity2\Build\Build\Listener\BuildListener;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BuildListenerFactory  implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $formateer = false  === strpos(php_sapi_name(), 'cli') ? new HtmlFormatter : new CliFormatter;
        return new BuildListener(
            $formateer,
            $serviceLocator->get('Log\BuildLogger'),
            $serviceLocator->get('Log\BuildErrorLogger')
        );
    }
}