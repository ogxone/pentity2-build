<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 24.04.2015
 * Time: 0:57
 */

namespace Pentity2\Build\Build\Listener;


use Formatter\AbstractFormatter;
use Formatter\FormatterInterface;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Log\Logger;

class BuildListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $_logger;
    private $_loggerError;
    /**
     * @var AbstractFormatter
    */
    private $_formatter;

    const EVENT_BUILD_DONE = 'build_done';
    const EVENT_BUILD_FAILURE = 'build_failure';
    const EVENT_BUILD_INIT_FAILURE = 'build_init_failure';
    const EVENT_BUILD_WARNING = 'build_warning';
    const EVENT_BUILD_INFO = 'build_info';

    const EVENT_CLEAR_DONE = 'clear_done';
    const EVENT_CLEAR_FAILURE = 'clear_failure';
    const EVENT_CLEAR_WARNING = 'clear_warning';

    public function __construct(FormatterInterface $formatter, Logger $logger, Logger $loggerError)
    {
        $this->_formatter = $formatter;
        $this->_logger = $logger;
        $this->_loggerError = $loggerError;
    }

    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(self::EVENT_BUILD_DONE, [$this, 'buildDone']);
        $this->listeners[] = $events->attach(self::EVENT_BUILD_FAILURE, [$this, 'buildFailure']);
        $this->listeners[] = $events->attach(self::EVENT_BUILD_INIT_FAILURE, [$this, 'buildInitFailure']);
        $this->listeners[] = $events->attach(self::EVENT_BUILD_WARNING, [$this, 'buildWarning']);
        $this->listeners[] = $events->attach(self::EVENT_BUILD_INFO, [$this, 'buildInfo']);

        $this->listeners[] = $events->attach(self::EVENT_CLEAR_DONE, [$this, 'clearDone']);
        $this->listeners[] = $events->attach(self::EVENT_CLEAR_FAILURE, [$this, 'clearFailure']);
        $this->listeners[] = $events->attach(self::EVENT_CLEAR_WARNING, [$this, 'clearWarning']);
    }

    public function buildDone(EventInterface $event)
    {
        $this->_logger->log(
            Logger::INFO,
            $this->_formatter
                ->setOption('color', 'green')
                ->format('BUILD OK! Component: %s', $event->getTarget()->getDescription())
        );
    }

    public function buildFailure(EventInterface $event)
    {
        $this->_loggerError->log(
            Logger::ERR,
            $this->_formatter
                ->setOption('color', 'red')
                ->format('BUILD FAILURE! Component: %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message'))
        );
    }

    public function buildInitFailure(EventInterface $event)
    {
        $this->_loggerError->log(
            Logger::ERR,
            $this->_formatter
                ->setOption('color', '#8B0000')
                ->format('BUILD FAILURE!Message: %s', $event->getParam('message'))
        );
    }

    public function buildWarning(EventInterface $event)
    {
        $this->_logger->log(
            Logger::NOTICE,
            $this->_formatter
                ->setOption('color', '#FF8C00')
                ->format('BUILD WARNING! Component: %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message'))
        );
    }

    public function buildInfo(EventInterface $event)
    {
        $this->_logger->log(
            Logger::INFO,
            $this->_formatter
                ->setOption('color', '#1E90FF')
                ->format('INFO: %s', $event->getParam('message'))
        );
    }

    public function clearDone(EventInterface $event)
    {
        $this->_logger->log(
            Logger::INFO,
            $this->_formatter
                ->setOption('color', 'green')
                ->format('CLEAR OK! Component: %s', $event->getTarget()->getDescription())
        );
    }

    public function clearFailure(EventInterface $event)
    {
        $this->_loggerError->log(
            Logger::ERR,
            $this->_formatter
                ->setOption('color', 'red')
                ->format('CLEAR FAILURE! Component: %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message'))
        );
    }

    public function clearInfo(EventInterface $event)
    {
        $this->_logger->log(
            Logger::INFO,
            $this->_formatter
                ->setOption('color', '#1E90FF')
                ->format('INFO: %s', $event->getParam('message'))
        );
    }

    public function clearWarning(EventInterface $event)
    {
        $this->_logger->log(
            Logger::NOTICE,
            $this->_formatter
                ->setOption('color', '#FF8C00')
                ->format('CLEAR WARNING! Component: %s. Message: %s', $event->getTarget()->getDescription(), $event->getParam('message'))
        );
    }
}