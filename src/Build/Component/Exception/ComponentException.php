<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.08.15
 * Time: 13:57
 */

namespace Pentity2\Build\Build\Component\Exception;


use Pentity2\Build\Build\Exception\BuildException;

class ComponentException extends BuildException
{

}