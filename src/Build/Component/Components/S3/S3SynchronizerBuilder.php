<?php
/**
 * Created by PhpStorm.
 * User: Макс
 * Date: 02.09.2015
 * Time: 18:09
 */

namespace Pentity2\Build\Build\Component\Components\S3;

use Pentity2\Build\Build\Component\AbstractBuilder;
use Pentity2\Utils\Param\Param;
use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Build\Build\S3ToolsTrait;

class S3SynchronizerBuilder extends AbstractBuilder
{
    use S3ToolsTrait;

    const SYNC_DONE_FILE = 'sync_done';

    private $_replacedImages = [];
    private $_extensionsForReplacement = ['css', 'js'];

    protected function _preBuildLogic(Array $params = [])
    {
        $this->_syncBegin();

        $dirsToClear = [
            $this->_getBuildPath() . 'images',
        ];

        foreach ($dirsToClear as $dir) {
            if (!file_exists($dir)) {
                continue;
            }
            Utils::clearDir($dir);
        }
    }

    protected function _buildLogic(Array $params = [])
    {
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($this->_getBuildPath()), \RecursiveIteratorIterator::SELF_FIRST
        );
        /*** @var $entry \SplFileInfo */
        foreach ($iterator as $entry) {
            if ($entry->isFile() && in_array($entry->getExtension(), $this->_extensionsForReplacement)) {
                $this->_replaceImages4S3($entry->getPath(), $entry->getFilename());
            }
        }

        $this->syncFolder2S3($this->_getBuildPath(), 'jslangs', true);
        $this->syncFolder2S3($this->_getBuildPath(), 'css', true);
        $this->syncFolder2S3($this->_getBuildPath(), 'js', true);
        Utils::recursiveCopyDir($this->_getImagesPath(), $this->_getBuildImagesPath(), true);
        $this->syncFolder2S3($this->_getBuildPath(), 'images');

        $this->_syncDone();
    }

    protected function _clearBuildLogic(Array $params = [])
    {
        $this->_preBuildLogic();
    }

    public function getDescription()
    {
        return 'Move files from build folder to S3 bucket';
    }

    private function _replaceImages4S3($path, $file)
    {
        $contentWithReplacedImages = preg_replace_callback(
            '`(/design/img/)(.*?).(png|jpg|gif)`si', [$this, '_replaceImagesInFile'], Utils::getFile($path . '/' . $file)
        );

        Utils::putFile($path, $file, $contentWithReplacedImages, true);
    }

    private function _replaceImagesInFile($matches)
    {
        $s3Config = Param::getArr('aws_s3', $this->_getConfig());
        if (!in_array($matches[2], $this->_replacedImages)) {
            $this->_replacedImages[] = $matches[2];
        }
        $keys = array_keys($this->_replacedImages, $matches[2]);

        $baseUrl = $this->_getCDNService()->generateCdnUrl(
            str_replace($this->_getImagesUrl() . '/', '', str_replace('.' .pathinfo($matches[0], PATHINFO_EXTENSION), '', $matches[0])),
            'images',
            array_shift($keys) % $s3Config['cdn']['count']
        );

        $filePath = realpath(PUBLIC_PATH . $matches[0]);

        if (!$filePath) {
            $this->_failure(sprintf("File \"%s\" does not exist in %s", PUBLIC_PATH . $matches[0], $filePath));
        }

        return $baseUrl . '_v' . md5(Utils::getFile($filePath)) . '.' . $matches[3];
    }

    private function _syncBegin()
    {
        if (file_exists($this->_getBuildPath() . '/' . self::SYNC_DONE_FILE)) {
            Utils::unlink($this->_getBuildPath() . '/' . self::SYNC_DONE_FILE);
        }
    }

    private function _syncDone()
    {
        Utils::putFile($this->_getBuildPath(), self::SYNC_DONE_FILE);
    }
}