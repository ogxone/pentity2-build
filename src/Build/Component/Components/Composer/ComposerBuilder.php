<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.08.15
 * Time: 19:57
 */

namespace Pentity2\Build\Build\Component\Components\Composer;


use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Utils\System\Exec;
use Pentity2\Build\Build\Component\AbstractBuilder;
use Pentity2\Build\Build\Exception\BuildException;
use Zend\Json\Json;

class ComposerBuilder extends AbstractBuilder
{
    protected function _buildLogic(Array $params = [])
    {
        if (!file_exists(ROOT_PATH . '/' . 'composer.lock')) {
            throw new BuildException(sprintf('composer.lock upset at %s', ROOT_PATH));
        }
        if (false === ($cwd = getcwd())) {
            throw new BuildException('Failed to get current working directory');
        }
        chdir(ROOT_PATH);
        $this->_configureAutoloader();
        $devOpt = $this->_env == 'production' ? '--no-dev' : '';
        $errors = Exec::withCommandLine(sprintf('php composer.phar install %s', $devOpt))->launch()->getErrors();
        chdir($cwd);
        if ('' !== $errors) {
            throw new BuildException($errors);
        }
        touch(ROOT_PATH . '/classmap_built');
    }

    protected function _clearBuildLogic(Array $aprams = [])
    {
        if (file_exists($path = ROOT_PATH . '/classmap_built')) {
            unlink($path);
        }
    }

    public function getDescription()
    {
        return 'Composer update';
    }

    private function _configureAutoloader()
    {
        if (!file_exists($path = ROOT_PATH . '/composer.json')) {
            $this->_notice(sprintf('Failed to find composer.json at %s. Creating one ...', ROOT_PATH));
            touch($path);
            $json = [];
        } else {
            $json = Json::decode(file_get_contents($path), Json::TYPE_ARRAY);
        }
        $json['autoloader'] = ['classmap' => [], 'psr-4' => []];
        $this->_configureModuleAutoloader($json);
        $this->_configureVendorAutoloader($json);
        file_put_contents($path, Json::encode($json));
    }

    private function _configureModuleAutoloader(Array &$json)
    {
        $di = new \DirectoryIterator(ROOT_PATH . '/module');
        foreach ($di as $module) {
            if ($module->isDot()) {
                continue;
            }
            $psr4Roots = [];
            foreach (['src', 'test'] as $namespaceRoot) {
                if (!file_exists($path = $module->getPathname() . sprintf('/%s/', $namespaceRoot))) {
                    $this->_notice(sprintf('Module %s does\'n have %s directory. Skipping ...', $module->getBasename(), $namespaceRoot));
                    continue;
                }
                $json['autoloader']['classmap'][] = $path;
                $psr4Roots[] = $path;
            }
            if ($psr4Roots) {
                $json['autoloader']['psr-4'][ucfirst($module->getBasename()) . '\\\\'] = $psr4Roots;
            }
        }
    }

    private function _configureVendorAutoloader(Array &$json)
    {
        $res = [];
        Utils::searchDir(VENDOR_PATH, 'src', $res);
        foreach ($res as $src) {
            $json['autoloader']['classmap'][] = $src . '/';
        }
    }
}