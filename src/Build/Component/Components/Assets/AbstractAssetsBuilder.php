<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 20:44
 */

namespace Pentity2\Build\Build\Component\Components\Assets;


use Pentity2\Build\Build\Exception\BuildException;
use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Utils\Param\Param;
use Pentity2\Build\Build\Component\AbstractBuilder;

abstract class AbstractAssetsBuilder extends  AbstractBuilder
{
    const GROUP_M = 1;
    const GROUP_MC = 2;
    const GROUP_MCA = 3;

    const ASSET_OPTION_TYPE_PLAIN = 'plain';
    const ASSET_OPTION_TYPE_ARRAY = 'array';

    protected $_assets;
    protected $_assetOptions;
    protected $_buildPath;
    protected $_allowedAssetTypes = ['css', 'js'];

    public function __construct($environment)
    {
        parent::__construct($environment);
        $this->_buildPath = ROOT_PATH . '/module/index/view/build';
        if (!is_dir($this->_buildPath)) {
            mkdir($this->_buildPath, 0777, true);
        }
    }

    public function &getAssets()
    {
        if (null === $this->_assets) {
            $this->_assets = Param::get('assets', $this->_getConfig());
        }
        return $this->_assets;
    }

    public function getAssetOptions()
    {
        if (null === $this->_assetOptions) {
            $this->_assetOptions = Param::get('assets_options', $this->_getConfig());
        }
        return $this->_assetOptions;
    }

    public function getAssetOption($name, $type = self::ASSET_OPTION_TYPE_PLAIN)
    {
        switch ($type) {
            case self::ASSET_OPTION_TYPE_ARRAY:
                return Param::getArr($name, $this->getAssetOptions());
            default:
                return Param::get($name, $this->getAssetOptions());
        }
    }

    protected function _preBuildLogic(Array $params = [])
    {
        $this->_copyVendorAssets();
        if (file_exists($this->_getBuildPath())) {
            Utils::clearDir($this->_getBuildPath());
        }
        foreach ($this->_allowedAssetTypes as $assetDir) {
            if (file_exists($this->_getBuildPath() . '/'  . $assetDir)) {
                Utils::clearDir($this->_getBuildPath());
            }
        }

        if ($this->_buildPath) {
            Utils::clearDir($this->_buildPath);
        }

        $buildLangFile = 'module/' . $this->_getBuildModule() . '/view/build/build.php';
        if (file_exists($buildLangFile)) {
            Utils::unlink($buildLangFile);
        }
    }

    protected function validate()
    {
        $assets = $assets = $this->getAssets();
        if (!is_array($assets) || empty($assets)) {
            throw new BuildException('No assets provided');
        }
    }

    /**
     *  Copies assets from it vendor dir to web accessible directory
     */
    protected function _copyVendorAssets()
    {
        $assets =  $this->getAssets();
        $types = $this->_allowedAssetTypes;
        $groups = array_keys($assets);
        foreach ($groups as $group) {
            foreach ($types as $type) {
                if (!isset($assets[$group][$type])) {
                    continue;
                }
                if (!is_array($assets[$group][$type])) {
                    $this->_notice(sprintf('Asset %s[%s] expected to be an array. Deleting...', $group, $type));
                    unset($assets[$group][$type]);
                }
                foreach ($assets[$group][$type] as $key => $assetData) {
                    if (!is_array($assetData)) {
                        $this->_notice(sprintf('Asset %s[%s][%s] expected to be an array. Deleting...', $group, $type, $key));
                        unset($assets[$group][$type][$key]);
                        continue;
                    }
                    if (!isset($assetData['vendor_path'])) {
                        continue;
                    }
                    $path = PUBLIC_PATH . Param::get('path', $assetData);
                    $vendorPath = Param::get('vendor_path', $assetData);
                    $allowOverwrite = Param::get('allow_overwrite', $assetData, true);

                    if (!$vendorPath) {
                        continue;
                    }
                    try {
                        Utils::copyRecursive($vendorPath, $path, $allowOverwrite);
                    } catch (\RuntimeException $e) {
                        $this->_notice(sprintf('File copying failed with message: %s. Deleting ...', $e->getMessage()));
                        unset($assets[$group][$type][$key]);
                    }
                }
            }
        }
    }

    protected function _clearBuildLogic(Array $params = [])
    {
        $this->_preBuildLogic();
    }

    protected function _getAssetCreatorForType($type)
    {
        $method = '_buildAsset' . trim(ucfirst($type));
        if (method_exists($this, $method)) {
            return $method;
        }
        $this->_notice(sprintf('Can\'t find asset creator function for given asset type %s. (skipping)', $type));
        return null;
    }

    protected function _getGroup($groupType, $assets)
    {
        $validGroups = array_filter(array_keys($assets), function($item) use ($groupType){
            if (count(explode('_', $item)) != $groupType) {
                return false;
            }
            return true;
        });
        foreach ($assets as $name => $asset) {
            if (false === (array_search($name, $validGroups))) {
                unset($assets[$name]);
            }
        }
        return $assets;
    }


    /**
     * @param $code
     * @throws \Exception
     * @return string
     */
    protected function _getHelperMethodByGroupCode($code)
    {
        switch ($code) {
            case self::GROUP_M:
                return 'getM';
            case self::GROUP_MC:
                return 'getMC';
            case self::GROUP_MCA:
                return 'getMCA';
        }
        throw new \Exception('Unknown group code ' . $code);
    }

    protected function _getGroupParent($name, Array $groups)
    {
        $nameChunks = explode('_', $name);
        if (count($nameChunks) <= 1) {
            return null;
        }
        $parents = [];
        foreach ($groups as $group) {
            $groupChunks = explode('_', $group);
            if (count($groupChunks) >= count($nameChunks)) { //this is child or equal resource, not parent
                continue;
            }
            $buf = $nameChunks;
            while (!empty($buf)) {
                array_pop($buf);
                if (
                    $group != $name && //omit self
                    count($groupChunks) == count(array_intersect($groupChunks, $buf)) //@todo test
                ) {
                    $parents[count($groupChunks)] = $group;
                }
            }
        }
        if (!empty($parents)) {
            return max($parents);
        }
        return null;
    }

    protected function _validateExtension($path)
    {
        $fileInfo = pathinfo($path);
        if (in_array($fileInfo['extension'], $this->_allowedAssetTypes)) {
            return true;
        } else {
            $this->_notice(
                sprintf(
                    'Disallowed file extension %s. Only %s are allowed. File %s not built.',
                    $fileInfo['extension'], implode(', ', $this->_allowedAssetTypes), $fileInfo['basename']
                )
            );
        }
        return false;
    }


    public function getDescription()
    {
        return 'Assets';
    }
}