<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 20:52
 */

namespace Pentity2\Build\Build\Component\Components\Assets;

use Pentity2\Utils\Param\Param;
use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Build\Build\S3ToolsTrait;
use Pentity2\Build\Build\Component\Components\S3\S3SynchronizerBuilder;

class MergeAssetsBuilder extends AbstractAssetsBuilder
{
    use S3ToolsTrait;

    protected function _buildLogic(Array $params = [])
    {
        $assets = $this->getAssets();
        $assetsMap = [];
        //creating assets merges
        foreach ($assets as $group => $entry) {
            foreach ($entry as $type => $files) {
                $filesToMerge = $this->_getBuildFiles($files);
                if ($groupMergedContent = $this->_buildGroupMerge($type, $filesToMerge)) {
                    $assetsMap[$group][$type][] = $this->_createAsset($group, $type, $groupMergedContent);
                }

                $filesPlain = $this->_getBuildFiles($files, true);
                foreach ($filesPlain as $file) {
                    if (is_readable($path = PUBLIC_PATH . Param::strict('path', $file))) {
                        if (is_file($path) && $this->_validateExtension($path)) {
                            $assetsMap[$group][$type][] = $this->_createPlainAsset(pathinfo($path)['filename'], $type, $path);
                        } else {
                            $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::SELF_FIRST);
                            /*** @var $elem \SplFileInfo */
                            foreach ($iterator as $elem) {
                                if ($elem->isFile() && $this->_validateExtension($elem->getRealPath())) {
                                    $assetsMap[$group][$type][] = $this->_createPlainAsset($elem->getBasename('.' . $type), $type, $elem->getRealPath());
                                }
                            }
                        }
                    } else {
                        $this->_notice(sprintf('Unreadable path %s (skipping)', $path));
                    }
                }
            }
        }

        //building main assets include files
        $include = '<?php ' . PHP_EOL;
        if (isset($assetsMap['main'])) {
            $mainInclude = '';
            foreach ($assetsMap['main'] as $assetType => $assetData) {
                if (null !== ($buildMethod = $this->_getAssetCreatorForType($assetType))) {
                    foreach ($assetData as $assetFile) {
                        $mainInclude .= $this->$buildMethod(basename($assetFile), $assetType);
                    }
                }
            }
            $excludeMainModules = $this->getAssetOption('exclude_main_modules', self::ASSET_OPTION_TYPE_ARRAY);
            if ($excludeMainModules) {
                $mainInclude = sprintf(
                    'if (!in_array($this->plugin("MCA")->getM(), %s)) {%s}' . PHP_EOL,
                    var_export($excludeMainModules, true),
                    PHP_EOL . $mainInclude . PHP_EOL
                );
            }
            $include .= $mainInclude;
            unset($assetsMap['main']);
        }

        //building other assets include files
        if ($assetsMap) {
            foreach ([self::GROUP_M, self::GROUP_MC, self::GROUP_MCA] as $groupCode) {      // helper loops
                foreach ($this->_getGroup($groupCode, $assetsMap) as $group => $entry) {    //
                    foreach ($entry as $assetType => $assetData) {
                        if (null !== ($buildMethod = $this->_getAssetCreatorForType($assetType))) {
                            $include .= PHP_EOL . sprintf('if ($this->plugin("MCA")->%s() == "%s"){', $this->_getHelperMethodByGroupCode($groupCode), $group) . PHP_EOL;
                            foreach ($assetData as $assetFile) {
                                $include .= $this->$buildMethod(basename($assetFile), $assetType);
                            }
                            $include .= '}' . PHP_EOL;
                        }
                    }
                }
            }
        }
        $include .= PHP_EOL . 'echo $this->headLink();' . PHP_EOL . ' ?>';

        $buildPath = 'module/' . $this->_getBuildModule() . '/view/build';
        if (!is_dir($buildPath)) {
            mkdir($buildPath, 0777, true);
        }
        Utils::putFile($buildPath, 'build.php', $include);
    }

    /**
     * Merges files from one group
     *
     * @param $type
     * @param array $files
     * @return null|string
     */
    private function _buildGroupMerge($type, Array $files)
    {
        if (!$this->_validateType($type)) {
            $this->_notice(sprintf('Unknown asset type %s. Only %s is allowed', $type, implode(', ', $this->_allowedAssetTypes)));
            return null;
        }
        if (!is_array($files)) {
            $this->_notice(sprintf('Asset group %s expected to be an array (skipping)', $type));
            return null;
        }
        $result = '';

        foreach ($files as $file) {
            if (is_readable($path = PUBLIC_PATH . Param::strict('path', $file))) {
                if (is_file($path) && $this->_validateExtension($path)) {
                        $this->_pushResult($result, $path);
                } else {
                    $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::SELF_FIRST);
                    foreach ($iterator as $entry) {
                        if ($entry->isFile() && $this->_validateExtension($entry->getRealPath())) {
                            $this->_pushResult($result, $entry->getPathname());
                        }
                    }
                }
            } else {
                $this->_notice(sprintf('Unreadable path %s (skipping)', $path));
            }
        }

        return $result;
    }

    private function _pushResult(&$result, $path)
    {
        $result .= sprintf(
            PHP_EOL . '/*=============%s===========*/%s', $this->_createAssetRelativePath($path), Utils::getFile($path)
        ) . PHP_EOL;
    }

    private function _createAssetRelativePath($path)
    {
        return preg_replace('|.*/public/(design/.*)|si', '$1', $path);
    }

    private function _validateType($type)
    {
        if (in_array($type, $this->_allowedAssetTypes)) {
            return true;
        }
        return false;
    }

    private function _createAsset($group, $type, $content)
    {
        if (!file_exists($dir = $this->_getBuildPath() . '/' . $type . '/')) {
            mkdir($dir, 0777, true);
        }
        $version = '_v' . md5($content);
        $fname = $group . $version . '.' . $type;

        Utils::putFile($dir, $fname, $content);

        return $this->_getBuildPath() . '/' . $type . '/' . $fname;
    }

    private function _createPlainAsset($filename, $type, $file)
    {
        $content = Utils::getFile($file);
        return $this->_createAsset($filename, $type, $content);
    }

    private function _getBuildFiles(Array $files, $plain = false) {
        return array_filter($files,
            function($file) use ($plain) {
                return $plain == Param::get('plain', $file);
            }
        );
    }

    protected function _buildAssetCss($cssFile, $type)
    {
        $include = '$this->headLink()' . PHP_EOL;
        $include .=
            sprintf("->appendStylesheet(" . '$this->CDN()->generateCdnUrl(' . "'%s', '%s', '%s', '%s', '%s'))",
                $cssFile, $this->_getBuildPath(), $this->_getAssetsBuildUrl(), S3SynchronizerBuilder::SYNC_DONE_FILE, $type
            );
        return $include . ';' . PHP_EOL;
    }

    protected function _buildAssetJs($jsFile, $type)
    {
        $include = ' $this->headScript()' . PHP_EOL;
        $include .=
            sprintf("->appendFile(" . '$this->CDN()->generateCdnUrl(' . "'%s', '%s', '%s', '%s', '%s'), 'text/javascript')" . PHP_EOL,
                $jsFile, $this->_getBuildPath(), $this->_getAssetsBuildUrl(), S3SynchronizerBuilder::SYNC_DONE_FILE, $type
            );
        return $include . ';' . PHP_EOL;
    }

}