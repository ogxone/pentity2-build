<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 18:12
 */

namespace Pentity2\Build\Build\Component\Components\Assets;


use Pentity2\Utils\ArrayUtils\ArrayUtils;
use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Utils\Param\Param;

class PlainAssetsBuilder extends AbstractAssetsBuilder
{
    protected function _buildLogic(Array $params = [])
    {
        $assets = $this->getAssets();
        $include = '<?php ' . PHP_EOL;
        $mainAssets = Param::getArr('main', $assets);
        if ($mainAssets) {
            $mainInclude = '';
            foreach ($mainAssets as $assetType => $main) {
                if (null !== ($buildMethod = $this->_getAssetCreatorForType($assetType))) {
                    $mainInclude .= $this->$buildMethod($main);
                }
            }
            $excludeMainModules = $this->getAssetOption('exclude_main_modules', self::ASSET_OPTION_TYPE_ARRAY);
            if ($excludeMainModules) {
                $mainInclude = sprintf(
                    'if (!in_array($this->plugin("MCA")->getM(), %s)) {%s}' . PHP_EOL,
                    var_export($excludeMainModules, true),
                    PHP_EOL . $mainInclude . PHP_EOL
                );
            }
            $include .= $mainInclude;
            unset($assets['main']);
        }
        if (!empty($assets)) {
            foreach ([self::GROUP_M, self::GROUP_MC, self::GROUP_MCA] as $groupCode) {
                foreach ($this->_getGroup($groupCode, $assets) as $group => $entry) {
                    foreach ($entry as $assetType => $assetData) {
                        if (null !== ($buildMethod = $this->_getAssetCreatorForType($assetType))) {
                            $include .= PHP_EOL . sprintf('if ($this->plugin("MCA")->%s() == "%s"){', $this->_getHelperMethodByGroupCode($groupCode), $group) . PHP_EOL;
                            $include .= $this->$buildMethod($assetData);
                            $include .= '}' . PHP_EOL;
                        }
                    }
                }
            }
        }
        $include .= PHP_EOL . 'echo $this->headLink();' . PHP_EOL . ' ?>';
        Utils::putFile($this->_buildPath, 'build.php', $include);
    }

    protected function _buildAssetCss($css, $version = 'microtime(true)', $versionRuntime = true)
    {
        if (!is_array($css)) {
            $css = [$css];
        }
        $include = ' $this->headLink()' . PHP_EOL;
        foreach ($css as $entry) {
            $file = Param::strict('path', $entry);
            if (!is_readable($path = PUBLIC_PATH . $file)) {
                $this->_notice(sprintf('Failed to find design file %s', $path));
                continue;
            }
            $buildAssetLineFunc = function(&$include, $file) use ($version, $versionRuntime){
                if ($versionRuntime) {
                    $versionString = $version;
                } else {
                    $versionString = "'{$version}'";
                }
                $include .= sprintf("->appendStylesheet('%s?v=' . {$versionString})" . PHP_EOL, $file);
            };
            if (is_dir($path)) {
                $this->_buildAssetFolder($include, $path, $buildAssetLineFunc);
            } elseif ($this->_validateExtension($path)) {
                $buildAssetLineFunc($include, $file);
            }
        }
        return $include . ';' . PHP_EOL;
    }

    protected function _buildAssetJs($js, $version = 'microtime(true)', $versionRuntime = true)
    {
        if (!is_array($js)) {
            $js = [$js];
        }
        $include = ' $this->headScript()' . PHP_EOL;
        foreach ($js as $entry) {
            $file = Param::strict('path', $entry);
            if (!is_readable($path = PUBLIC_PATH . $file)) {
                $this->_notice(sprintf('Failed to find design file %s', $path));
                continue;
            }
            $buildAssetLineFunc = function(&$include, $file) use ($version, $versionRuntime){
                if ($versionRuntime) {
                    $versionString = $version;
                } else {
                    $versionString = "'{$version}'";
                }
                $include .= sprintf("->appendFile('%s?v=' . {$versionString}, 'text/javascript')" . PHP_EOL, $file);
            };
            if (is_dir($path)) {
                $this->_buildAssetFolder($include, $path, $buildAssetLineFunc);
            } elseif ($this->_validateExtension($path)) {
                $buildAssetLineFunc($include, $file);
            }
        }
        return $include . ';' . PHP_EOL;
    }

    protected function _buildAssetFolder(&$include, $path, callable $buildAssetLineFunc)
    {
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::CURRENT_AS_FILEINFO),
            \RecursiveIteratorIterator::CHILD_FIRST
        );
        $files = ArrayUtils::iteratorToArray($iterator);
        ksort($files, SORT_NUMERIC);
        foreach ($files as $entry) {
            if ($entry->isFile() && $this->_validateExtension($entry->getRealPath())) {
                $a = str_replace(PUBLIC_PATH, '',$entry->getPathname());
                $buildAssetLineFunc($include, str_replace(PUBLIC_PATH, '',$entry->getPathname()));
            }
        }
    }
}