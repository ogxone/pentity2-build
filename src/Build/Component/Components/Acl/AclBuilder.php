<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 21:05
 */

namespace Pentity2\Build\Build\Component\Components\Acl;


use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Build\Build\Component\AbstractBuilder;

class AclBuilder extends AbstractBuilder
{
    protected function _buildLogic(Array $params = [])
    {
        $aclMap = [];
        $modules = new \DirectoryIterator('module');
        foreach ($modules as $module) {
            /** #var $module \SplFileInfo*/
            if (!$module->isDir() || $module->isDot()) {
                continue;
            }
            $moduleSrc = strtolower(trim($module->getFilename()));
            array_push($aclMap, $moduleSrc);
            try {
                $controllers = new \DirectoryIterator($module->getPathname() . '/src/Controller');
                foreach ($controllers as $controller) {
                    if ($controller->isDir() || $controller->isDot()) {
                        continue;
                    }
                    if (false !== ($pos = strrpos($controllerFileName = $controller->getBasename(), '.'))) {
                        $controllerFileName = substr($controllerFileName, 0, $pos);
                    }
                    $controllerSrc = substr(lcfirst($controllerFileName), 0, -10);
                    $controllerSrc = $moduleSrc . '_'
                        . preg_replace_callback('/([A-Z])/', function(Array $m){return '-' . strtolower($m[1]);}, $controllerSrc);
                    //build actions
                    $controllerClassName = ucfirst($moduleSrc) . '\Controller\\' . $controllerFileName;

                    if (!class_exists($controllerClassName)) {
                        $this->_notice(sprintf('ACL build: class file %s was found, but class autoloader failed', $controllerClassName));
                        continue;
                    }
                    array_push($aclMap, $controllerSrc);
                    $ref = new \ReflectionClass($controllerClassName);

                    foreach ($ref->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
                        if ($method->class != $ref->name) { // skipp parent class methods
                            continue;
                        }
                        /**@var $method \ReflectionMethod*/
                        if (substr($method->name, -6) == 'Action') {
                            $action = substr($method->name, 0, -6);
                            $action = preg_replace_callback('/([A-Z])/', function(Array $m){return '-' . strtolower($m[1]);}, $action);
                            array_push($aclMap, $controllerSrc . '_' . trim($action));
                        }
                    }
                }
            } catch (\UnexpectedValueException $e) { // no Controller folder
                $this->_notice(sprintf('ACL build:%s', $e->getMessage()));
            }
        }
        file_put_contents('acl.php', sprintf('<?php return %s;?>', var_export($aclMap, true)));
    }

    protected function _clearBuildLogic(Array $params = [])
    {
        Utils::unlink($path = ROOT_PATH . '/acl.php');
    }

    public function getDescription()
    {
        return 'Acl list';
    }
}