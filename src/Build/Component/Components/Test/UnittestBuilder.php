<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 11.08.15
 * Time: 14:48
 */

namespace Pentity2\Build\Build\Component\Components\Test;


use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Utils\System\Exec;
use Pentity2\Build\Build\Component\AbstractBuilder;
use Pentity2\Build\Build\Exception\BuildException;
use Zend\Json\Json;

class UnittestBuilder extends AbstractBuilder
{
    private $_testFileName;
    private $_testSuitesName = 'phpunit_tests';
    private $_logFileName;

    public function __construct($environment)
    {
        parent::__construct($environment);
        $this->_testFileName = ROOT_PATH . '/' . 'phpunit.build.xml';
        $this->_logFileName = ROOT_PATH . '/' . 'phpunit.log.build.json';
    }

    protected function _buildLogic(Array $params = [])
    {
        $phpunit = new \DOMDocument;
        $phpunit->appendChild($rootNode = $phpunit->createElement('phpunit'));
        $rootNode->appendChild($suites = $phpunit->createElement('testsuites'));
        $suite = $phpunit->createElement('testsuite');
        $suite->setAttribute('name', $this->_testSuitesName);

        $modules = new \DirectoryIterator(ROOT_PATH . '/module');
        foreach ($modules as $module) {
            if (is_dir($dir = $module->getPathname() . '/test')) {
                $dirNode = $phpunit->createElement('directory');
                $dirNode->appendChild($phpunit->createTextNode($dir));
                $suite->appendChild($dirNode);
            }
        }
        $suites->appendChild($suite);
        file_put_contents($this->_testFileName, $phpunit->saveXML());
        $this->_runTests();
        $this->_analizeResults();
    }

    protected function _clearBuildLogic(Array $params = [])
    {
        Utils::unlink($this->_testFileName);
    }

    public function getDescription()
    {
        return 'Unit tests';
    }

    private function _runTests()
    {
        Exec::withCommandLine(sprintf('php phpunit.phar  -c %s --log-json %s', $this->_testFileName, $this->_logFileName))
            ->launch();
    }

    private function _analizeResults()
    {
        if (!file_exists($this->_logFileName)) {
            throw new BuildException(sprintf('Failed to find phpunit result logs. (Check your permissions)'));
        }
        $log = Json::decode($this->_loadLogs(), Json::TYPE_ARRAY);
        $failedTests = '';
        foreach ($log as $event) {
            if ($event['event'] !== 'test') {
                continue;
            }
            if ($event['status'] == 'fail') {
                $failedTests .= sprintf('Following test has failed: %s with message %s. Stack trace was %s',
                      $event['test'], $event['message'], Json::encode($event['trace']));
            }
        }
        if ($failedTests) {
            throw new BuildException($failedTests);
        }
    }

    private function _loadLogs()
    {
        $rawData = file_get_contents($this->_logFileName);
        $jsonData = '[' . preg_replace_callback('/(})({)/', function($m){
                return $m[1] . ',' . $m[2];
            }, $rawData) . ']';
        return $jsonData;
    }
}