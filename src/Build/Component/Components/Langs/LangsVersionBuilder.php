<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 21:06
 */

namespace Pentity2\Build\Build\Component\Components\Langs;

use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Build\Build\Component\Components\S3\S3SynchronizerBuilder;

class LangsVersionBuilder extends LangsBuilder
{
    protected function _buildLangJs($locale, $jsDir, $jsTable)
    {
        $content = sprintf('i18n = %s;', json_encode($jsTable));
        $jsFileVersion = $locale . '_v' . md5($content) . '.js';
        Utils::putFile($jsDir, $jsFileVersion, $content);

        return sprintf(
            'if ($this->Lang() == "%s") {' . PHP_EOL
            . '$this->headScript()->appendFile($this->CDN()->generateCdnUrl("%s", "%s", "%s", "%s", "%s"), "text/javascript");' . PHP_EOL
            . '}' . PHP_EOL  ,
            $locale, $jsFileVersion, $this->_getBuildPath(), $this->_getAssetsBuildUrl(), S3SynchronizerBuilder::SYNC_DONE_FILE, 'jslangs'
        );
    }

    protected function _prepareJsFolder()
    {
        if (!file_exists($jsDir = PUBLIC_PATH . '/design/build/jslangs')) {
            mkdir($jsDir, 0777, true);
        }

        return $jsDir;
    }

    public function getDescription()
    {
        return 'Langs with version for production server';
    }
}