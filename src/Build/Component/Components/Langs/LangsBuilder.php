<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 21:06
 */

namespace Pentity2\Build\Build\Component\Components\Langs;


use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Build\Build\Component\AbstractBuilder;
use Pentity2\Build\Build\Exception\RuntimeBuildException;
use Pentity2\Build\Build\S3ToolsTrait;

class LangsBuilder extends AbstractBuilder
{
    use S3ToolsTrait;

    protected function _preBuildLogic(Array $params = [])
    {
        $dirsToClear = [
            DATA_PATH . '/build/langs',
            PUBLIC_PATH . '/design/js/build/langs',
            $this->_getBuildPath() . 'langs',
        ];

        foreach ($dirsToClear as $dir) {
            if (!file_exists($dir)) {
                continue;
            }
            Utils::clearDir($dir);
        }

        $buildLangFile = 'module/' . $this->_getBuildModule() . '/view/build/lang_build.php';
        if (file_exists($buildLangFile)) {
            Utils::unlink($buildLangFile);
        }
    }

    protected function _buildLogic(Array $params = [])
    {
        $langs = $this->_getRepoFactory()
            ->createRepo('Lang')
            ->fetchAll();
        if (null === $langs) {
            throw new RuntimeBuildException(sprintf('Langs was not built. Langs table is supposed not to be empty', ENVIRONMENT));
        }
        $langsMap = [];
        //crete langs map
        foreach ($langs as $lang) {
            /** @var $lang EntityInterface*/
            if (!empty($lang->locale) && !empty($lang->code) && !empty($lang->phrase)) {
                $langsMap
                [$lang->getField('locale')]
                [$lang->getField('code')] = $lang->getField('phrase');
            }
        }
        //saving langs
        if (!file_exists($dir = DATA_PATH . '/build/langs')) {
            mkdir($dir, 0777, true);
        }

        $jsDir = $this->_prepareJsFolder($dir);

        $include = '<?php ' . PHP_EOL;
        foreach ($langsMap as $locale => $table) {
            Utils::putFile($dir, $locale . '.php', sprintf('<?php return %s;?>', var_export($table, true)));
            $jsTable = array_map(function($i){
                if (is_int($i[0])) {
                    $i = '_' . $i;
                }
                $i = preg_replace('|\s|is', ' ', $i);
                return $i;
            }, $table);

            $include .= $this->_buildLangJs($locale, $jsDir, $jsTable);
        }
        $include .= PHP_EOL . '?>';
        $buildPath = 'module/' . $this->_getBuildModule() . '/view/build';
        if (!is_dir($buildPath)) {
            mkdir($buildPath, 0777, true);
        }
        Utils::putFile($buildPath, 'lang_build.php', $include);
    }

    protected function _clearBuildLogic(Array $params = [])
    {
        $this->_preBuildLogic();
    }

    protected function _buildLangJs($locale, $jsDir, $jsTable)
    {
        Utils::putFile($jsDir, $locale . '.js', sprintf('i18n = %s;', json_encode($jsTable)));

        return sprintf(
            'if ($this->Lang() == "%s") {' . PHP_EOL
            . '$this->headScript()->appendFile("%s?t=" . microtime(true), "text/javascript");' . PHP_EOL
            . '}' . PHP_EOL  ,
            $locale, $this->_getAssetsUrl(). '/js/build/langs/' . $locale . '.js'
        );
    }

    protected function _prepareJsFolder()
    {
        if (!file_exists($jsDir = PUBLIC_PATH . '/design/js/build/langs')) {
            mkdir($jsDir, 0777, true);
        }

        return $jsDir;
    }

    public function getDescription()
    {
        return 'Langs';
    }
}