<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 21:11
 */

namespace Pentity2\Build\Build\Component\Components\Templatemap;

use Pentity2\Utils\Filesystem\Utils;
use Pentity2\Utils\System\Exec;
use Pentity2\Build\Build\Component\AbstractBuilder;

class TemplatemapBuilder extends AbstractBuilder
{
    protected function _buildLogic(Array $params = [])
    {
        $em = $this->getEventManager();
        $di = new \DirectoryIterator(ROOT_PATH . '/module');
        foreach ($di as $module) {
            /**@var $di \SplFileInfo*/
            if (
                $module->isDot() ||
                !$module->isDir() ||
                in_array($module->getBasename(), $this->_getConfig()['template_map_exclude_modules'])
            ) {
                continue;
            }

            if (
                file_exists($viewPath = $module->getPathname()) &&
                file_exists($viewPath . '/view')
            ) {
                $this->_info(sprintf('Building templatemap for %s module', $module->getBasename()));
                $errors = Exec::withCommandLine($str = sprintf('ROOT_PATH=%s;export ROOT_PATH;php %s/vendor/bin/templatemap_generator.php -w -l "%s"  -v "%s"', ROOT_PATH, ROOT_PATH, $viewPath, $viewPath. '/view'))
                    ->launch()
                    ->getErrors();
                if ('' !== $errors) {
                    $this->_notice($errors);
                }
            }
        }
    }

    protected function _clearBuildLogic(Array $params = [])
    {
        $di = new \DirectoryIterator(ROOT_PATH . '/module');
        foreach ($di as $module) {
            /**@var $di \SplFileInfo*/
            if ($module->isDot() || !$module->isDir()) {
                continue;
            }
            if (file_exists($viewPath = $module->getPathname() . '/template_map.php')) {
                try {
                    Utils::unlink($viewPath);
                } catch (\UnexpectedValueException $e) {
                    $this->_failure($e->getMessage());
                }
            }
        }
    }

    public function getDescription()
    {
        return 'template map';
    }
}