<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 21:18
 */

namespace Pentity2\Build\Build\Component\Components;


use Pentity2\Build\Build\Component\AbstractBuilder;

abstract class CustomBuild extends  AbstractBuilder
{
    protected function _createId($value, $delimiter = '_')
    {
        return strtolower(
            preg_replace(['|\s+|is' ,sprintf('|[^a-zA-Z0-9%s]+|is', $delimiter)], [$delimiter, ''], $value)
        );
    }
}