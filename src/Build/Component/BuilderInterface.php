<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 23.04.2015
 * Time: 23:51
 */

namespace Pentity2\Build\Build\Component;


interface BuilderInterface
{
    public function build(Array $params);
    public function clearBuild(Array $params);
}