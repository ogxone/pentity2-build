<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 17.06.15
 * Time: 17:25
 */

namespace Pentity2\Build\Build\Component;


use Pentity2\Utils\Collection\Collection;
use Pentity2\Build\Build\Exception\BuildException;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AggregateBuilder extends Collection implements BuilderInterface
{
    use ServiceLocatorAwareTrait;

    public function build(Array $params = [])
    {
        foreach ($this as $builder) {
            $builder->build($params);
        }
    }

    public function clearBuild(Array $params = [])
    {
        foreach ($this as $builder) {
            $builder->clearBuild($params);
        }
    }


    public function getDescription()
    {
        throw new BuildException('Not implemented yet');
    }
}