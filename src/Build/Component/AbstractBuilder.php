<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 17.06.15
 * Time: 19:03
 */

namespace Pentity2\Build\Build\Component;


use Pentity2\Build\Build\Exception\BuildException;
use Pentity2\Build\Build\Exception\RuntimeBuildException;
use Pentity2\Build\Build\Listener\BuildListener;
use Pentity2\Build\DescriptionAwareInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class AbstractBuilder implements
    BuilderInterface, DescriptionAwareInterface, EventManagerAwareInterface, ServiceLocatorAwareInterface
{
    use EventManagerAwareTrait, ServiceLocatorAwareTrait;

    protected $_buildBlocks = [];
    protected $_configBuildPath;
    protected $_environment;
    protected $_env;

    public function __construct($environment)
    {
        $this->_environment = $environment;
        $this->_eol = false !== strpos(php_sapi_name(), 'cli') ? PHP_EOL : '<br/>';
    }

    protected function _getBuildPath()
    {
        return PUBLIC_PATH . '/design/build';
    }

    protected function _getBuildAssetsTmpPath()
    {
        return PUBLIC_PATH . '/design/build/tmp';
    }

    protected function _getBuildImagesPath()
    {
        return PUBLIC_PATH . '/design/build/images';
    }

    protected function _getImagesPath()
    {
        return PUBLIC_PATH . '/design/img';
    }

    protected function _getBuildModule()
    {
        return 'index';
    }

    protected function _getAssetsUrl()
    {
        return '/design';
    }

    protected function _getAssetsBuildUrl()
    {
        return '/design/build';
    }

    protected function _getImagesUrl()
    {
        return '/design/img';
    }

    protected function _getConfigBuildPath()
    {
        return ROOT_PATH . '/config/autoload/build';
    }

    protected function _getConfig()
    {
        return $this->getServiceLocator()->get('Config');
    }

    protected function _getCDNService()
    {
        return $this->getServiceLocator()->get('CDN');
    }

    protected function _getRepoFactory()
    {
        return $this->getServiceLocator()->get('RepoFactory');
    }

    public function build(Array $params = [])
    {
        $em = $this->getEventManager();
        try {
            $em->trigger(BuildListener::EVENT_BUILD_INFO, $this, ['message' => sprintf('Starting build component %s ...', $this->getDescription())]);
            $this->_validate($params);
            $this->_preBuildLogic($params);
            $this->_buildLogic($params);
            $this->_postBuildLogic($params);
            $em->trigger(BuildListener::EVENT_BUILD_DONE, $this);
        } catch(RuntimeBuildException $e) {
            $em->trigger(BuildListener::EVENT_BUILD_WARNING, $this, ['message' => $e->getMessage()]);
        } catch (BuildException $e) {
            $em->trigger(BuildListener::EVENT_BUILD_FAILURE, $this, ['message' => $e->getMessage()]);
        }
    }

    public function clearBuild(Array $params = [])
    {
        $em = $this->getEventManager();
        try {
            $em->trigger(BuildListener::EVENT_BUILD_INFO, $this, ['message' => sprintf('Starting clear component %s ...', $this->getDescription())]);
            $this->_clearBuildLogic($params);
            $em->trigger(BuildListener::EVENT_CLEAR_DONE, $this);
        } catch(RuntimeBuildException $e) {
            $em->trigger(BuildListener::EVENT_CLEAR_WARNING, $this, ['message' => $e->getMessage()]);
        } catch (BuildException $e) {
            $em->trigger(BuildListener::EVENT_CLEAR_FAILURE, $this, ['message' => $e->getMessage()]);
        }
    }

    protected function _notice($message)
    {
        $this->_trig(BuildListener::EVENT_BUILD_WARNING, $message);
    }

    protected function _info($message)
    {
        $this->_trig(BuildListener::EVENT_BUILD_INFO, $message);
    }

    protected function _failure($message)
    {
        $this->_trig(BuildListener::EVENT_BUILD_FAILURE, $message);
    }

    protected function _trig($event, $message, \Exception $exception = null)
    {
        $this->getEventManager()
            ->trigger($event, $this, [
                'message' => $message,
                'exception' => $exception
            ]);
    }
    protected function _validate(Array $params = []) {}
    protected function _preBuildLogic(Array $params = []) {}
    protected function _postBuildLogic(Array $params = []) {}
    abstract protected function _buildLogic(Array $params = []);
    abstract protected function _clearBuildLogic(Array $params = []);
}