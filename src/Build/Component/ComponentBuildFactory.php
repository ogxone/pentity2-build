<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.08.15
 * Time: 18:15
 */

namespace Pentity2\Build\Build\Component;


use Pentity2\Build\Build\Component\Exception\ComponentException;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class ComponentBuildFactory implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private $_namespaces = [
        'Pentity2\Build\Build\Component\Components',
        'Build\Infrastructure\Service\Build\Component\Components'
    ];

    public function createBuildComponent($name, $type, $environment)
    {
        $folder = trim(ucfirst($name));
        $type = trim(ucfirst($type)) . 'Builder';
        foreach ($this->_namespaces as $namespace) {
            if (class_exists($class = $namespace . '\\' . $folder . '\\' . $type)) {
                $component = new $class($environment);
                $component->setServiceLocator($this->getServiceLocator());
                $component->setEventManager($this->getServiceLocator()->get('BuildEventManager'));
                return $component;
            }
        }
        throw new ComponentException(sprintf('Undefined component type %s', $name));
    }
}