<?php
return [
    'service_manager' => [
        'invokables' => [
            'Build\Factory' => 'Pentity2\Build\Build\Factory\BuildFactory',
            'Build\ComponentFactory' => 'Pentity2\Build\Build\Component\ComponentBuildFactory',

            'Build\CacheCleanerFactory' => 'Pentity2\Build\Cache\Factory\CacheCleanerFactory'
        ],
        'factories' => [
            'Build\BuildListener' => 'Pentity2\Build\Build\Listener\Factory\BuildListenerFactory',
            'Build\CacheCleanerListener' => 'Pentity2\Build\Cache\Listener\Factory\CacheCleanerListenerFactory',
        ]
    ],
];